package asymmetric_encryption;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Objects;

import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.SecretKey;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHParameterSpec;

import org.apache.commons.codec.binary.Base64;

/**
 * 对称加密算法--密钥交换算法
 * @author gaopengfei
 *
 */

public class JavaDH {

	private static String src = "面向对象编程，object-oriented！@#*5"; // 需要加密的原始字符串
	
	public static void main(String[] args) throws Exception {
		System.out.println("初始字符串：" + src);
		jdkDH();
	}
	
	/** JDK实现密钥交换算法 */
	public static void jdkDH() throws Exception{
		
		//1.初始化发送方密钥
		KeyPairGenerator senderKeyPairGenerator = KeyPairGenerator.getInstance("dh");
		senderKeyPairGenerator.initialize(512);//密钥长度512
		KeyPair senderKeyPair = senderKeyPairGenerator.generateKeyPair();
		byte[] senderPublicKeyEnc = senderKeyPair.getPublic().getEncoded();//发送方公钥（需要把这个发送给）
		
		//2.初始化接收方密钥
		KeyFactory receiverkeyFactory = KeyFactory.getInstance("dh");
		X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(senderPublicKeyEnc);
		PublicKey receiverPublicKey = receiverkeyFactory.generatePublic(x509EncodedKeySpec);
		DHParameterSpec dhParameterSpec = ((DHPublicKey)receiverPublicKey).getParams();
		KeyPairGenerator receiverKeyPairGenerator = KeyPairGenerator.getInstance("dh");
		receiverKeyPairGenerator.initialize(dhParameterSpec);
		KeyPair receiverKeyPair = receiverKeyPairGenerator.generateKeyPair();
		PrivateKey receiverPrivateKey = receiverKeyPair.getPrivate();
		
		//3.密钥构建
		KeyAgreement receiverkeyAgreement = KeyAgreement.getInstance("dh");
		receiverkeyAgreement.init(receiverPrivateKey);
		receiverkeyAgreement.doPhase(receiverPublicKey, true);
		SecretKey receiverDesKey = receiverkeyAgreement.generateSecret("des");
		byte[] receiverPublicKeyEnc = receiverKeyPair.getPublic().getEncoded();
		
		KeyFactory senderKeyFactory = KeyFactory.getInstance("dh");
		x509EncodedKeySpec = new X509EncodedKeySpec(receiverPublicKeyEnc);
		PublicKey senderPublicKey = senderKeyFactory.generatePublic(x509EncodedKeySpec);
		KeyAgreement senderKeyAgreement = KeyAgreement.getInstance("dh");
		senderKeyAgreement.init(senderKeyPair.getPrivate());
		senderKeyAgreement.doPhase(senderPublicKey, true);
		SecretKey senderDesKey = senderKeyAgreement.generateSecret("des");
		if (Objects.equals(senderDesKey,receiverDesKey)) {
			System.out.println("双方密钥相同");
		}
		
		//4.加密
		Cipher cipher = Cipher.getInstance("des");
		cipher.init(Cipher.ENCRYPT_MODE, senderDesKey);
		byte[] result = cipher.doFinal(src.getBytes());
		System.out.println("密钥交换算法加密：" + Base64.encodeBase64String(result));
		
		//5.解密
		cipher.init(Cipher.DECRYPT_MODE, receiverDesKey);
		result = cipher.doFinal(result);
		System.out.println("密钥交换算法解密：" + new String(result));
	}
}
