package symmetric_encryption;

import java.security.Key;
import java.security.SecureRandom;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class JavaPBE {

	private static String src = "面向对象编程，object-oriented！@#*5"; // 需要加密的原始字符串
	
	public static void main(String[] args) throws Exception {
		System.out.println("原始字符串：" + src);
		jdkPBE();
		bouncyCastlePBE();
	}

	public static void jdkPBE() throws Exception{
		
		//1.初始化盐
		SecureRandom secureRandom = new SecureRandom();//强加密随机数生成器
		byte[] salt= secureRandom.generateSeed(8);//产生盐必须是8位
		
		//2.口令与密钥
		String password = "root";
		PBEKeySpec pbeKeySpec = new PBEKeySpec(password.toCharArray());//密钥转换的对象
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBEWithMD5AndDES");//实例化密钥转换工厂
		Key key = factory.generateSecret(pbeKeySpec);//由工厂产生key
		
		//3.加密
		PBEParameterSpec parameterSpec = new PBEParameterSpec(salt, 100);//PBE输入参数的材料，盐，迭代100次
		Cipher cipher = Cipher.getInstance("PBEWithMD5AndDES");
		cipher.init(Cipher.ENCRYPT_MODE, key, parameterSpec);
		byte[] result = cipher.doFinal(src.getBytes());
		System.out.println("jdk PBE加密：" + Base64.encodeBase64String(result));
		
		//4.解密
		cipher.init(Cipher.DECRYPT_MODE, key, parameterSpec);
		result = cipher.doFinal(result);
		System.out.println("jdk PBE解密：" + new String(result));
	}
	
	public static void bouncyCastlePBE() throws Exception{
		
		Security.addProvider(new BouncyCastleProvider());//添加到provider
		
		SecureRandom secureRandom = new SecureRandom();
		byte[] salt = secureRandom.generateSeed(8);
		
		String password = "admin";
		PBEKeySpec pbeKeySpec = new PBEKeySpec(password.toCharArray());
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBEWithMD5AndDES","BC");//指定provider
		Key key = factory.generateSecret(pbeKeySpec);
		
		PBEParameterSpec parameterSpec = new PBEParameterSpec(salt, 50);
		Cipher cipher = Cipher.getInstance("PBEWithMD5AndDES");
		cipher.init(Cipher.ENCRYPT_MODE, key,parameterSpec);
		byte[]result = cipher.doFinal(src.getBytes());
		
		System.out.println("bc PBE加密：" + Base64.encodeBase64String(result));
		
		cipher.init(Cipher.DECRYPT_MODE, key, parameterSpec);
		result = cipher.doFinal(result);
		System.out.println("bc PBE解密：" + new String(result));
	}
}
