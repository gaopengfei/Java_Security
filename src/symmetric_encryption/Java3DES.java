package symmetric_encryption;

import java.security.SecureRandom;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Java3DES {

	private static String src = "面向对象编程，object-oriented！@#*5"; // 需要加密的原始字符串
	
	public static void main(String[] args) throws Exception {
		System.out.println("原始字符串：" + src + "\n");
		jdk3DES();
		bouncyCastle3DES();
	}

	/** jdk实现3DES */
	public static void jdk3DES() throws Exception{
		//1.生成key
		KeyGenerator keyGenerator = KeyGenerator.getInstance("DESede");//密钥生成器
//		keyGenerator.init(new SecureRandom()); //可以用它生成一个默认长度的key
		keyGenerator.init(112);//指定密钥长度为112位
		SecretKey secretKey = keyGenerator.generateKey();//用密钥生成器生成密钥
		byte[] byteKeys = secretKey.getEncoded();//得到密钥的byte数组
		
		//2.key转换
		DESedeKeySpec deSedeKeySpec = new DESedeKeySpec(byteKeys);
		SecretKeyFactory factory = SecretKeyFactory.getInstance("DESede");//秘密密钥工厂
		SecretKey convertSecretKey = factory.generateSecret(deSedeKeySpec);
		
		//3.加密
		Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, convertSecretKey);//加密模式
		byte[]result = cipher.doFinal(src.getBytes());
		System.out.println("jdk DES3加密：\n" + Hex.encodeHexString(result));
		
		//4.解密
		cipher.init(Cipher.DECRYPT_MODE, convertSecretKey);//解密模式
		result = cipher.doFinal(result);
		System.out.println("jdk DES3解密：\n" + new String(result) + "\n");
	}
	
	/** bc实现3DES */
	public static void bouncyCastle3DES() throws Exception{
		
		Security.addProvider(new BouncyCastleProvider());
		
		//1.生成key
		KeyGenerator keyGenerator = KeyGenerator.getInstance("desede","BC");//密钥生成器[指定provider]
		keyGenerator.init(new SecureRandom());//指定密钥的长度为默认
		SecretKey secretKey = keyGenerator.generateKey();//用密钥生成器生成密钥
		byte[] byteKeys = secretKey.getEncoded();//得到密钥的byte数组
		
		//2.key转换
		DESedeKeySpec deSedeKeySpec = new DESedeKeySpec(byteKeys);
		SecretKeyFactory factory = SecretKeyFactory.getInstance("desede");//秘密密钥工厂
		SecretKey convertSecretKey = factory.generateSecret(deSedeKeySpec);
		
		//3.加密
		Cipher cipher = Cipher.getInstance("desede/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, convertSecretKey);//加密模式
		byte[]result = cipher.doFinal(src.getBytes());
		System.out.println("bc DES3加密：\n" + Hex.encodeHexString(result));
		
		//4.解密
		cipher.init(Cipher.DECRYPT_MODE, convertSecretKey);//解密模式
		result = cipher.doFinal(result);
		System.out.println("bc DES3解密：\n" + new String(result) + "\n");
		
	}
}
