package symmetric_encryption;

import java.security.Key;
import java.security.SecureRandom;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 * JDK默认只支持128为密钥的AES加密，如果想要实现192和256位的AES加密，需要获得jdk的无政策限制文件
 * @author gaopengfei
 *
 */

public class JavaAES {

	private static String src = "面向对象编程，object-oriented！@#*5"; // 需要加密的原始字符串
	
	public static void main(String[] args) throws Exception {
		System.out.println("初始字符串:" + src + "\n");
		jdkAES();
		bouncyCastleAES();
	}

	public static void jdkAES() throws Exception{
		
		//1.生成key
		KeyGenerator keyGenerator = KeyGenerator.getInstance("aes");
		keyGenerator.init(256);//初始化key的长度，默认只支持128，要想使用192和256，则必须获得jdk的无限制文件
		SecretKey secretKey = keyGenerator.generateKey();//生成key
		byte[] keyBytes = secretKey.getEncoded();//得到key的字节数组
		
		//2.key的转换
		Key key = new SecretKeySpec(keyBytes, "aes");
		
		//3.加密
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");//加解密方式+工作模式+填充方式
		cipher.init(Cipher.ENCRYPT_MODE, key);//以加密模式初始化
		byte[] result = cipher.doFinal(src.getBytes());
		System.out.println("JDK AES加密：" + Base64.encodeBase64String(result));
		
		//4.解密
		cipher.init(Cipher.DECRYPT_MODE, key);
		result = cipher.doFinal(result);
		System.out.println("JDK AES解密：" + new String(result));
	}
	
	/** Bouncy Castle实现AES加密 */
	public static void bouncyCastleAES() throws Exception{
		
		Security.addProvider(new BouncyCastleProvider());
		KeyGenerator keyGenerator = KeyGenerator.getInstance("aes", "BC");
		keyGenerator.init(new SecureRandom());
		SecretKey secretKey = keyGenerator.generateKey();
		byte[] keyBytes = secretKey.getEncoded();
		
		Key key = new SecretKeySpec(keyBytes, "aes");
		
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5padding");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] result = cipher.doFinal(src.getBytes());
		System.out.println("bc AES加密：" + Base64.encodeBase64String(result));
		
		cipher.init(Cipher.DECRYPT_MODE, key);
		result = cipher.doFinal(result);
		System.out.println("bc AES解密：" + new String(result));
	}
}
