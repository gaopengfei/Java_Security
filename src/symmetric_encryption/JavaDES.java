package symmetric_encryption;

import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class JavaDES {

	private static String src = "面向对象编程，object-oriented！@#*5"; // 需要加密的原始字符串
	
	public static void main(String[] args) throws Exception {
		System.out.println("原始字符串：\n" + src);
		jdkDES();
		bouncyCastleDES();
	}

	/** jdk实现DES加密 */
	public static void jdkDES() throws Exception{
		
		//1.生成key
		KeyGenerator keyGenerator = KeyGenerator.getInstance("des");//密钥生成器
		keyGenerator.init(56);//指定密钥长度为56位
		SecretKey secretKey = keyGenerator.generateKey();//用密钥生成器生成密钥
		byte[] byteKeys = secretKey.getEncoded();//得到密钥的byte数组
		
		//2.key转换
		DESKeySpec desKeySpec = new DESKeySpec(byteKeys);
		SecretKeyFactory factory = SecretKeyFactory.getInstance("des");//秘密密钥工厂
		SecretKey convertSecretKey = factory.generateSecret(desKeySpec);
		
		//3.加密
		Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, convertSecretKey);//加密模式
		byte[]result = cipher.doFinal(src.getBytes());
		
		System.out.println("jdk DES加密：\n" + Hex.encodeHexString(result));
		
		//4.解密
		cipher.init(Cipher.DECRYPT_MODE, convertSecretKey);//解密模式
		result = cipher.doFinal(result);
		System.out.println("jdk DES解密：\n" + new String(result));
	}
	
	/** Bouncy Castle实现DES加密 */
	public static void bouncyCastleDES() throws Exception{
		
		Security.addProvider(new BouncyCastleProvider());//增加provider
		
		//1.生成key
		KeyGenerator keyGenerator = KeyGenerator.getInstance("des","BC");//密钥生成器，提供商指定为bouncyCastle（默认是sun的JCE）
		keyGenerator.init(56);//指定key长度为56位
		keyGenerator.getProvider();
		SecretKey secretKey = keyGenerator.generateKey();//用密钥生成器生成密钥
		byte[] byteKeys = secretKey.getEncoded();//得到密钥的byte数组
		
		//2.key转换
		DESKeySpec desKeySpec = new DESKeySpec(byteKeys);
		SecretKeyFactory factory = SecretKeyFactory.getInstance("des");//密钥工厂
		SecretKey convertSecretKey = factory.generateSecret(desKeySpec);
		
		//3.加密
		Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, convertSecretKey);//加密模式
		byte[] result = cipher.doFinal(src.getBytes());
		System.out.println("bc DES加密：\n" + Hex.encodeHexString(result));
		
		//4.解密
		cipher.init(Cipher.DECRYPT_MODE, convertSecretKey);//解密模式
		result = cipher.doFinal(result);
		System.out.println("bc DES解密：\n" + new String(result));
	}
}
