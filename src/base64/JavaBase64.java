package base64;

import java.io.IOException;

import org.apache.commons.codec.binary.Base64;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * 该类是Base64算法的实现
 */

public class JavaBase64 {

	private static String src = "面向对象编程，object-oriented！@#*5"; // 需要加密的原始字符串
	
	public static void main(String[] args) throws IOException {
		System.out.println("原始字符串：\t\t\t" + src);
		System.out.println();
		jdkBase64();
		System.out.println();
		commonsCodecBase64();
		System.out.println();
		bouncyCastleBase64();
	}

	/** JDK实现Base64编码 */
	public static void jdkBase64() throws IOException{
		//如果这里没有出现sun的base64可以从Build Path中删除然后添加
		BASE64Encoder encoder = new BASE64Encoder();
		String encode = encoder.encode(src.getBytes());//编码
		System.out.println("JDK实现的base64编码：\t\t" + encode);
		
		BASE64Decoder decoder = new BASE64Decoder();
		String decode = new String(decoder.decodeBuffer(encode));//解码
		System.out.println("JDK实现的base64解码：\t\t" + decode);
	}
	
	/** Apache的 Commons Codec实现base64编码 */
	public static void  commonsCodecBase64() {
		byte[] encodeBytes = Base64.encodeBase64(src.getBytes());
		System.out.println("Commons Codec实现base64编码：\t" + new String(encodeBytes));
		
		byte[] decodeBytes = Base64.decodeBase64(encodeBytes);
		System.out.println("Commons Codec实现base64解码：\t" + new String(decodeBytes));
	}
	
	/**Bouncy Castle实现base64编码 */
	public static void bouncyCastleBase64() {
		byte[] encodeBytes = org.bouncycastle.util.encoders.Base64.encode(src.getBytes());
		System.out.println("Bouncy Castle实现base64编码：\t"+new String(encodeBytes));
		
		byte[] decodeBytes = org.bouncycastle.util.encoders.Base64.decode(encodeBytes);
		System.out.println("Bouncy Castle实现base64解码：\t"+new String(decodeBytes));
	}
}
