package message_digest;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.crypto.digests.MD5Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;


public class JavaHMAC {

	private static String src = "object-oriente"; // 需要加密的原始字符串
	
	public static void main(String[] args) throws Exception {
		
		System.out.println("原始字符串：" + src + "\n");
		jdkHmacMD5();
		bouncyCastleHmacMD5();
	}
	
	public static void jdkHmacMD5() throws Exception{
		
		//1.得到密钥
//		KeyGenerator keyGenerator = KeyGenerator.getInstance("HmacMD5"); 
//		SecretKey secretKey = keyGenerator.generateKey();//生成密钥
//		byte[] key = secretKey.getEncoded();//获得密钥
		byte[] key = Hex.decodeHex("1234567890".toCharArray());//必须是16进制的字符，且长度是偶数
		
		//2.还原密钥
		SecretKey restoreSecretKey = new SecretKeySpec(key, "hmacMD5");
		
		//3.信息摘要
		Mac mac = Mac.getInstance(restoreSecretKey.getAlgorithm());//实例化mac
		mac.init(restoreSecretKey);//初始化mac
		byte[] hmacMD5Bytes = mac.doFinal(src.getBytes());//执行摘要
		
		System.out.println("jdkHmacMD5:\t" + Hex.encodeHexString(hmacMD5Bytes));
	}
	
	public static void bouncyCastleHmacMD5() {
		
		HMac hmac = new HMac(new MD5Digest());
		//生成密钥的时候以1234567890为基准
		hmac.init(new KeyParameter(org.bouncycastle.util.encoders.Hex.decode("1234567890")));//必须是16进制的字符，且长度是偶数
		hmac.update(src.getBytes(), 0, src.getBytes().length);
		
		// 执行摘要
		byte[]hmacMDdBytes = new byte[hmac.getMacSize()];
		hmac.doFinal(hmacMDdBytes, 0);
		
		System.out.println("bcHmacMD5:\t"+org.bouncycastle.util.encoders.Hex.toHexString(hmacMDdBytes));
	}
	
}
