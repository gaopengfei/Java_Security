package message_digest;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.MD2Digest;
import org.bouncycastle.crypto.digests.MD5Digest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class JavaMD {

	private static String src = "object-oriente"; // 需要加密的原始字符串
	
	public static void main(String[] args) throws NoSuchAlgorithmException {
		System.out.println("原始字符串：" + src + "\n");
		
		jdkMD5();
		bouncyCastleMD5();
		commonsCodecMD5();
		System.out.println();
		
		jdkMD2();
		bouncyCastleMD2();
		commonsCodecMD2();
		System.out.println();
		
		bouncyCastleMD4();
	}

	/** jdk实现MD5加密 */
	public static void jdkMD5() throws NoSuchAlgorithmException {
		
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] md5Bytes = md.digest(src.getBytes());
		System.out.println("JDK MD5:" + Hex.encodeHexString(md5Bytes));//利用第三方包将byte数组转化为16进制字符串
	}
	
	/** jdk实现MD2加密 */
	public static void jdkMD2() throws NoSuchAlgorithmException {
		
		MessageDigest md = MessageDigest.getInstance("md2");
		byte[] md5Bytes = md.digest(src.getBytes());
		System.out.println("JDK MD2:" + Hex.encodeHexString(md5Bytes));
	}
	
	/** Bouncy Castle实现MD4加密 */
	public static void bouncyCastleMD4() throws NoSuchAlgorithmException{
		/*通过这种方式给JDK动态添加一个provider,就可以通过这种方式获得JDK本身不支持的MD4了*/
		Security.addProvider(new BouncyCastleProvider());
		
		MessageDigest md = MessageDigest.getInstance("md4");
		byte[] md4Bytes = md.digest(src.getBytes());
		System.out.println("bc MD4:\t" + org.bouncycastle.util.encoders.Hex.toHexString(md4Bytes));
	}
	
	/** Bouncy Castle实现MD5加密 */
	public static void bouncyCastleMD5(){
		
		Digest digest = new MD5Digest();
		digest.update(src.getBytes(), 0, src.getBytes().length);
		byte[]md5Bytes = new byte[digest.getDigestSize()];
		digest.doFinal(md5Bytes, 0);
		System.out.println("bc MD5:\t" + org.bouncycastle.util.encoders.Hex.toHexString(md5Bytes));
	}
	
	/** Bouncy Castle实现MD2加密 */
	public static void bouncyCastleMD2(){
		Digest digest = new MD2Digest();
		digest.update(src.getBytes(), 0, src.getBytes().length);
		byte[]md2Bytes = new byte[digest.getDigestSize()];
		digest.doFinal(md2Bytes, 0);
		System.out.println("bc MD2:\t" + org.bouncycastle.util.encoders.Hex.toHexString(md2Bytes));
		
	}
	
	/** Commons Codec 实现MD5加密*/
	public static void commonsCodecMD5() {
		System.out.println("cc MD5:\t" + DigestUtils.md5Hex(src.getBytes()));
	}
	
	/** Commons Codec 实现MD2加密*/
	public static void commonsCodecMD2() {
		System.out.println("cc MD2:\t" + DigestUtils.md2Hex(src.getBytes()));
	}
}
