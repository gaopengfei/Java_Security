#Java_Security

### JAVA安全实现三种方式：
    1.JDK 2.Commons Codec 3.Bouncy Castle

====
####  一。非对称加密算法：asymmetric_encryption
    1.DH（密钥交换算法） 2.RSA（基于大数分解） 3.ElGamal（基于离散对数）

####  二。Base64：base64
    1.JDK实现 2.Common Codes实现 3.Bouncy Castle实现

####  三。消息摘要算法：message_digest
    1.MD5 2.SHA 3.MAC（消息认证码）

####  四。数字签名:JDK实现  digital_signature
    1.RSA 2.DSA（数字签名算法） 3.ECDSA（椭圆曲线数字签名）

####  五。对称加密算法：symmetric_encryption
    1.DES 2.3DES 3.AES 4.PBE（基于口令的加密）
    
====
####   非对称加密算法中“ElGamal” ，的异常问题：
    对于：“Illegal key size or default parameters”异常，是因为美国的出口限制，Sun通过权限文件（local_policy.jar、US_export_policy.jar）做了相应限制。因此存在一些问题。

    Java 7 无政策限制文件：[java官方下载](http://www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html) 本 UnlimitedJCEPolicyJDK7.zip 包已经下载到本项目的ext目录下。
    
    windows的系统可能需要如下操作：
    切换到%JDK_Home%\jre\lib\security目录下，解压UnlimitedJCEPolicyJDK7.zip，对应覆盖local_policy.jar和US_export_policy.jar两个文件。如果你安装了jre，则还需要替换jre/lib/security下的两个对应文件。
